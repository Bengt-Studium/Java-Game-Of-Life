package gol.config;

import gol.data.IMap;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

@SuppressWarnings("serial")
public class Configurator extends JDialog {

	private JPanel contentPane;
	private JTextField widthText;
	private JTextField heightText;
	private JTextField zoomText;
	private JTextField fpsText;
	
	private int widthValue, heightValue, zoomValue;
	private double fpsValue;
	private boolean valuesReady;

	public Configurator() {
		valuesReady = false;
		setTitle("Game Of Life");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 210, 180);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{100, 100};
		gbl_contentPane.rowHeights = new int[]{20, 20, 20, 20, 20, 20};
		gbl_contentPane.columnWeights = new double[]{0.0, 0.0};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
		contentPane.setLayout(gbl_contentPane);
		
		JLabel widthLabel = new JLabel("Width");
		widthLabel.setToolTipText("The width of the map in units.");
		GridBagConstraints gbc_widthLabel = new GridBagConstraints();
		gbc_widthLabel.fill = GridBagConstraints.BOTH;
		gbc_widthLabel.insets = new Insets(0, 0, 5, 5);
		gbc_widthLabel.gridx = 0;
		gbc_widthLabel.gridy = 0;
		contentPane.add(widthLabel, gbc_widthLabel);
		
		widthText = new JTextField();
		widthLabel.setLabelFor(widthText);
		widthText.setText(Integer.toString(IMap.DEFAULT_WIDTH));
		GridBagConstraints gbc_widthText = new GridBagConstraints();
		gbc_widthText.fill = GridBagConstraints.BOTH;
		gbc_widthText.insets = new Insets(0, 0, 5, 0);
		gbc_widthText.gridx = 1;
		gbc_widthText.gridy = 0;
		contentPane.add(widthText, gbc_widthText);
		widthText.setColumns(10);
		
		JLabel heightLabel = new JLabel("Height");
		heightLabel.setToolTipText("The height of the map in units.");
		GridBagConstraints gbc_heightLabel = new GridBagConstraints();
		gbc_heightLabel.fill = GridBagConstraints.BOTH;
		gbc_heightLabel.insets = new Insets(0, 0, 5, 5);
		gbc_heightLabel.gridx = 0;
		gbc_heightLabel.gridy = 1;
		contentPane.add(heightLabel, gbc_heightLabel);
		
		heightText = new JTextField();
		heightLabel.setLabelFor(heightText);
		heightText.setText(Integer.toString(IMap.DEFAULT_HEIGHT));
		GridBagConstraints gbc_heightText = new GridBagConstraints();
		gbc_heightText.fill = GridBagConstraints.BOTH;
		gbc_heightText.insets = new Insets(0, 0, 5, 0);
		gbc_heightText.gridx = 1;
		gbc_heightText.gridy = 1;
		contentPane.add(heightText, gbc_heightText);
		heightText.setColumns(10);
		
		JLabel zoomLabel = new JLabel("Zoom");
		zoomLabel.setToolTipText("The width and height of a unit in pixels.");
		GridBagConstraints gbc_zoomLabel = new GridBagConstraints();
		gbc_zoomLabel.fill = GridBagConstraints.BOTH;
		gbc_zoomLabel.insets = new Insets(0, 0, 5, 5);
		gbc_zoomLabel.gridx = 0;
		gbc_zoomLabel.gridy = 2;
		contentPane.add(zoomLabel, gbc_zoomLabel);
		
		zoomText = new JTextField();
		zoomLabel.setLabelFor(zoomText);
		zoomText.setText("5");
		GridBagConstraints gbc_zoomText = new GridBagConstraints();
		gbc_zoomText.fill = GridBagConstraints.BOTH;
		gbc_zoomText.insets = new Insets(0, 0, 5, 0);
		gbc_zoomText.gridx = 1;
		gbc_zoomText.gridy = 2;
		contentPane.add(zoomText, gbc_zoomText);
		zoomText.setColumns(10);
		
		JLabel fpsLabel = new JLabel("FPS");
		fpsLabel.setToolTipText("The desired maximum FPS.");
		GridBagConstraints gbc_fpsLabel = new GridBagConstraints();
		gbc_fpsLabel.fill = GridBagConstraints.BOTH;
		gbc_fpsLabel.insets = new Insets(0, 0, 5, 5);
		gbc_fpsLabel.gridx = 0;
		gbc_fpsLabel.gridy = 3;
		contentPane.add(fpsLabel, gbc_fpsLabel);
		
		fpsText = new JTextField();
		fpsLabel.setLabelFor(fpsText);
		fpsText.setText("10.0");
		GridBagConstraints gbc_fpsText = new GridBagConstraints();
		gbc_fpsText.fill = GridBagConstraints.BOTH;
		gbc_fpsText.insets = new Insets(0, 0, 5, 0);
		gbc_fpsText.gridx = 1;
		gbc_fpsText.gridy = 3;
		contentPane.add(fpsText, gbc_fpsText);
		fpsText.setColumns(10);
		
		final JLabel statusLabel = new JLabel("");
		statusLabel.setForeground(Color.RED);
		GridBagConstraints gbc_statusLabel = new GridBagConstraints();
		gbc_statusLabel.gridwidth = 2;
		gbc_statusLabel.insets = new Insets(0, 0, 5, 5);
		gbc_statusLabel.gridx = 0;
		gbc_statusLabel.gridy = 4;
		contentPane.add(statusLabel, gbc_statusLabel);
		
		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		GridBagConstraints gbc_cancelButton = new GridBagConstraints();
		gbc_cancelButton.fill = GridBagConstraints.BOTH;
		gbc_cancelButton.insets = new Insets(0, 0, 0, 5);
		gbc_cancelButton.gridx = 0;
		gbc_cancelButton.gridy = 5;
		contentPane.add(cancelButton, gbc_cancelButton);
		
		JButton okButton = new JButton("OK");
		okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String result = parseValues();
				if (result != null) {
					statusLabel.setText(result);
				} else {
					statusLabel.setText("");
					valuesReady = true;
					dispose();
				}
			}
		});
		GridBagConstraints gbc_okButton = new GridBagConstraints();
		gbc_okButton.fill = GridBagConstraints.BOTH;
		gbc_okButton.gridx = 1;
		gbc_okButton.gridy = 5;
		contentPane.add(okButton, gbc_okButton);
	}
	
	public void run() {
		setVisible(true);
	}
	
	private String parseValues() {
		try {
			widthValue = Integer.parseInt(widthText.getText());
		} catch(NumberFormatException nfe) {
			return "can't parse width";
		}
		try {
			heightValue = Integer.parseInt(heightText.getText());
		} catch(NumberFormatException nfe) {
			return "can't parse heigth";
		}
		try {
			zoomValue = Integer.parseInt(zoomText.getText());
		} catch(NumberFormatException nfe) {
			return "can't parse zoom";
		}
		try {
			fpsValue = Double.parseDouble(fpsText.getText());
		} catch(NumberFormatException nfe) {
			return "can't parse fps";
		}
		
		if (widthValue < 3)
			return "Width >= 3";
		if (heightValue < 3)
			return "Height >= 3";
		if (zoomValue < 1)
			return "zoom >= 1";
		if (fpsValue < 0)
			return "FPS >= 0";
		
		int screenWidth = Toolkit.getDefaultToolkit().getScreenSize().width;
		int screenHeight = Toolkit.getDefaultToolkit().getScreenSize().height;
		if (widthValue * zoomValue > screenWidth)
			return "Too wide for your screen";
		if (heightValue * zoomValue > screenHeight)
			return "Too high for your screen";
		return null;
	}
	
	public int getWidthValue() {
		return widthValue;
	}

	public int getHeightValue() {
		return heightValue;
	}

	public int getFieldSizeValue() {
		return zoomValue;
	}

	public double getFpsValue() {
		return fpsValue;
	}
	
	public boolean areValuesReady() {
		return valuesReady;
	}
}
