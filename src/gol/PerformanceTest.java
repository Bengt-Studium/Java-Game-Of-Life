package gol;

import gol.data.BooleanMap;
import gol.data.IMap;

public class PerformanceTest {
	public static final int DEFAULT_GENERATIONS = 10000;
	
	private IMap map;
	
	private int numGenerations;
	private long elapsedTime = -1;
	
	public PerformanceTest() {
		this(IMap.DEFAULT_WIDTH, IMap.DEFAULT_HEIGHT, DEFAULT_GENERATIONS);
	}
	
	public PerformanceTest(int width, int height) {
		this(width, height, DEFAULT_GENERATIONS);
	}
	
	public PerformanceTest(int width, int height, int numGenerations) {
		map = new BooleanMap(width, height);
		this.numGenerations = numGenerations > 0 ? numGenerations : DEFAULT_GENERATIONS;
	}
	
	public void run() {
		long startTime = System.nanoTime();
		for (int i = 0; i < numGenerations; i++) {
			map.calcNextGeneration();
		}
		elapsedTime = System.nanoTime() - startTime;
	}
	
	public void printResults() {
		if (elapsedTime == -1) {
			System.out.println("The test did not run yet!");
			return;
		}
		double seconds = (double)elapsedTime / 1000000000d;
		double generationsPerSecond = (double)numGenerations / seconds;
		System.out.println("Test parameters: width = " + map.getWidth() + ", height = " + map.getHeight());
		System.out.println("calculated " + numGenerations + " generations in " + seconds + " seconds");
		System.out.println("=> " + generationsPerSecond + " generations per second");
	}
}
