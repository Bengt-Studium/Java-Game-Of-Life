package gol.data;

public class BooleanMap implements IMap {
	protected boolean[][] map;
	
	public BooleanMap(int width, int height) {
		// use default values if the given values are invalid
		width = width >= MIN_WIDTH ? width : DEFAULT_WIDTH;
		height = height >= MIN_HEIGHT ? height : DEFAULT_HEIGHT;
		
		// add a border around the map; makes calculations faster
		width += 2;
		height += 2;
		
		map = new boolean[width][height];
	}
	
	@Override
	public boolean get(int x, int y) {
		// I assume, that x and y are correct for efficiency reasons
		return map[x+1][y+1];
	}

	@Override
	public void set(int x, int y, boolean value) {
		// I assume, that x and y are correct for efficiency reasons
		map[x+1][y+1] = value;
	}

	@Override
	public int getWidth() {
		return map.length - 2;
	}

	@Override
	public int getHeight() {
		return map[0].length - 2;
	}

	@Override
	public void calcNextGeneration() {
		boolean[][] nextGenMap = new boolean[getWidth()+2][getHeight()+2];
		int neighbourCount;
		
		for (int x = 1; x < map.length - 1; x++) {
			for (int y = 1; y < map[0].length - 1; y++) {
				neighbourCount = countNeighbours(x, y);
				nextGenMap[x][y] =
						neighbourCount == 3 || (map[x][y] && neighbourCount == 2);
			}
		}
		
		map = nextGenMap;
	}
	
	protected int countNeighbours(int x, int y) {
		// I assume, that x and y are correct for efficiency reasons
		int count = 0;
		int xl = x-1, xr = x+1, yt = y-1, yb = y+1;
		
		// top neighbours
		if (map[xl][yt])
			count++;
		if (map[x][yt])
			count++;
		if (map[xr][yt])
			count++;
		
		// left and right neighbour
		if (map[xl][y])
			count++;
		if (map[xr][y])
			count++;
		
		// bottom neightbours
		if (map[xl][yb])
			count++;
		if (map[x][yb])
			count++;
		if (map[xr][yb])
			count++;
		
		return count;
	}
}
