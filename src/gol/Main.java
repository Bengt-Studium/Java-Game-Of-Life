package gol;

import gol.config.Configurator;
import gol.data.BooleanMap;
import gol.data.IMap;
import gol.ui.J2DGUI;

public class Main {

	public static void main(String[] args) {
		runConfigurator();
//		runPerformanceTest();
	}
	
	private static void runConfigurator() {
		final Configurator cfg = new Configurator();
		cfg.run();
		
		// HACK: using events instead of this breaks the J2DGUI
		// waiting for the user input to be finished...
		while (!cfg.areValuesReady()) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		runSimulation(cfg.getWidthValue(), cfg.getHeightValue(), cfg.getFieldSizeValue(), cfg.getFpsValue());
	}
	
	private static void runSimulation(int width, int height, int fieldSize, double fps) {
		IMap map = new BooleanMap(width, height);
		fillMapRandomly(map, 0.6);
		J2DGUI gui = new J2DGUI(map.getWidth(), map.getHeight(), fieldSize);
		gui.render(map);
		
		// fps == 0 means no fps restriction
		long nspf;
		if (fps != 0)
			nspf = (long) (1000000000.0 / fps);
		else
			nspf = 0;
		
		long time, timeDiff;
		time = System.nanoTime();
		while (gui.isAvailable()) {
			map.calcNextGeneration();
			gui.render(map);
			timeDiff = System.nanoTime() - time;
			if (nspf - timeDiff > 0) {
				try {
					Thread.sleep((nspf - timeDiff) / 1000000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			time = System.nanoTime();
		}

		gui.dispose();
	}
	
	private static void fillMapRandomly(IMap map, double ratio) {
		for (int x = 0; x < map.getWidth(); x++) {
			for (int y = 0; y < map.getHeight(); y++) {
				map.set(x, y, Math.random() > ratio);
			}
		}
	}

	private static void runPerformanceTest() {
		PerformanceTest test = new PerformanceTest(400, 300, 10000);
		test.run();
		test.printResults();
	}
}
